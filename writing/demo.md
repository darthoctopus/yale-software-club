---
header-includes:
    - \input{macros.tex}
    - \usepackage{mathptmx,txfonts,tikz,bm}
date: \today
documentclass: aastex631
classoption: astrosymb, twocolumn, tighten, twocolappendix
mathspec: false
colorlinks: true
citecolor: xlinkcolor # don't override AASTeX default
urlcolor: xlinkcolor
bibliography: biblio.bib
biblio-style: aasjournal
---

\shorttitle{Demo Paper}
\title{Demo Paper: Pandoc as an AASTeX Build System}
\input{preamble}
\begin{abstract}
I propose the use of \texttt{pandoc} as a document preparation tool.
\keywords{Asteroseismology (73), Stellar oscillations (1617), Computational methods (1965), Theoretical techniques (2093)}
\end{abstract}

# Introduction

Markdown is an *excellent* document preparation format. In conjunction with the `pandoc` tool by John G McFarlane, transpilation to \LaTeX{} is possible, and raw \TeX{} may be included in the document if a pdf file is the final build target. Math may also be included enclosed in standard delimiters. Both inline math --- e.g. $\left( 1 - \exp\left[{h \nu \over k T}\right] \right)^{-1}$ --- and display math: $$I_\nu = {2 h \nu^3 \over c^2}{1 \over 1 - \exp\left[{h \nu \over k T}\right]},$$
are supported.

# Conclusion

I also include a custom bibstyle to mimic the standard AAS typesetting that presents both `doi` and `ads` identifiers by way of coloured hyperlinks.

\software{NumPy \citep{numpy}, SciPy stack \citep{scipy}, AstroPy \citep{astropy:2013,astropy:2018}, Pandas \citep{pandas}, \mesa\ \citep{mesa_paper_1,mesa_paper_2,mesa_paper_3,mesa_paper_4,mesa_paper_5}, \gyre\ \citep{townsend_gyre_2013}.}

<!--\bibliography{biblio.bib}-->
