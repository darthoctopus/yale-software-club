{
"translatorID":"7b53e1eb-2f7d-46ae-bad4-06e4b8f3ca05",
"translatorType":2,
"label":"Zotero PDF URI",
"creator":"Joel Ong",
"target":"html",
"minVersion":"6.0",
"maxVersion":"",
"priority":200,
"inRepository":false,
"displayOptions":{"exportCharset":"UTF-8"},
"lastUpdated":"2022-08-22 18:10:00"
}

// Put this in ~/Zotero/translators

function doExport() {
	var item;
	while(item = Zotero.nextItem()) {
		for (attachment of item.attachments) {
			if (attachment.contentType == 'application/pdf') {
				itemkey = attachment.uri.split("/").pop();
				// Zotero.write("zotero://open-pdf/library/items/"+ itemkey +"/?page=1");	// this isn't working for some reason
				Zotero.write("["+ item.creators[0].lastName+"+ ("+ZU.strToDate(item.date).year+")](zotero://open-pdf/0_"+ itemkey +"/1)");
			}
		}
	}
}
