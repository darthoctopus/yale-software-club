# Joel's Science Workflow

A short presentation to the Yale Astronomy Software Club on 23/8/2022 describing the workflow that I used for my PhD research. In summary:

- Note-taking based on Markdown, typically incorporated into Jupyter notebooks. I also include hyperlinks to either (1) other notebooks, or (2) Zotero PDF entries, using custom URI handlers. Elements of this scheme are strongly inspired by [Giles Castel's workflow](https://castel.dev/post/research-workflow/) for his math PhD.
- Document preparation in Markdown, compiled to AASTeX (and then to PDFs) via pandoc. For this I had to customise the default latex template a little to only output authors if any are specified in the YAML header, since AASTeX specifically requires authors to only be declared after `\begin{document}`. I also have a custom `aasjournal.bst` with magenta links for DOIs and blue links for ADS URIs.
- Presentations also authored in Markdown, compiled to either Beamer PDFs (for short conference talks) or revealjs HTML presentations, depending on context. I have a custom Yale beamer theme that I made in first year which I'll never be able to use again, unfortunately.


## Caveats

- In order to get custom URI schemes to work in Jupyter notebooks, I had to manually patch one of the javascript files that came with `notebook` to add the custom schemes to the `allowedSchemes` property of the default HTML sanitiser --- see [here](https://github.com/jupyterlab/jupyterlab/issues/7384). I expect this to be fixed in the near future.