---
title: My Scientific Workflow
subtitle: Software Club
author: Joel Ong

# if beamer
classoption:
    - 12pt
    - aspectratio=169
    - dvipsnames
header-includes:
    - \RequirePackage{luatex85}
    - \usepackage{amsmath,amssymb,amsfonts, graphicx, float, braket, tikz,fontspec, subcaption, appendixnumberbeamer}
    - \definecolor{reddish}{rgb}{0.77, 0, 0}
    - \definecolor{forestgreen}{rgb}{0.09, 0.44, 0.09}
    - \definecolor{bluish}{rgb}{0.09, 0.20, 0.64}
    - \hypersetup{unicode, linkcolor=reddish, citecolor=forestgreen, urlcolor=bluish}
    - \usepackage[super]{nth}
    - \institute[Yale Astro]{Department of Astronomy, Yale University}
#    - \metroset{progressbar=head, block=fill}
    - \usecolortheme[title]{yale2}
    - \usepackage{multimedia}
    - \usepackage{pgfpages, pgffix}
#    - \setbeameroption{show notes on second screen}
natbiboptions: authoryear, round
colorlinks: true
# theme: metropolis
# date: \today
subject: Subject of Talk

# if revealjs
theme: white
date: 23rd Aug 2022
css: aux.css
transitionSpeed: fast
hash: true
---

## Overview

- Doing science
- Writing about science
- Presenting science

# Doing Science

## Reference Management

- I use [Zotero](https://www.zotero.org/) for reference management and occasionally annotations.

- Zotero has some (not very well-documented) URI handling functionality that can simplify your life

- You can link to pages in individual PDF files by looking up in the database: here's a link to [Page 22 of Aerts+ (2018)](zotero://open-pdf/0_FQPKSW3L/22)


## Jupyter Notebooks

I use [`pyneapple`](http://gitlab.com/darthoctopus/pyneapple) to interact with jupyter notebooks on my [local filesystem](file-manager:///home/joel/scratch).

<!-- Put the filemanager.desktop file in ~/.local/share/applications and the filemanager.sh shell somewhere in your path. -->

# Writing About Science

## Document preparation with `pandoc`

- I author my papers in Markdown and use `pandoc` to compile them to TeX, accommodating idiosyncracies of AASTeX, which in turn compiles to PDFs.
- [Demonstration](file-manager:///home/joel/scratch/software/writing/demo.md)

# Presenting Science

## Pandoc $\to$ Slides

- Generally speaking I like to also prepare my slides in Markdown and use `pandoc` to turn them into either [`beamer`](file-manager:///home/joel/scratch/software/slides/Slides.pdf) PDF files, or [`revealjs`](Slides.html#/pandoc-to-slides) HTML slides.
- [Here's the source code for these slides](file-manager:///home/joel/scratch/software/slides/Slides.md)

---

:::::::::::::: {.columns .small}
::: {.column width="40%"}
### Beamer

- Self-contained PDF file (unless you have animations)
- Works everywhere, good for conferences
- Limited extensibility

:::
::: {.column width="60%"}
### `revealjs`

- Animations are painless
- You can do insane interactive things that are impossible with PDF files
- Essentially impossible to redistribute, requires internet access
- Terrible for conferences

:::
::::::::::::::

## Interactivity

In `revealjs` you can include arbitrary HTML content in iframes. This content will not appear when the slides are built to beamer PDFs.

---

<iframe src="http://portfolio.hyad.es/talks/Figure%20Bank/toys/multiplet.html" width="720" height="450" style="border: 2px solid black"></iframe>

---

![Animations in revealjs don't necessarily work in beamer](http://portfolio.hyad.es/talks/Figure%20Bank/evol_echelle/echelle.webm)

## Blocks

Note that raw $\mathrm{\LaTeX}$ is omitted when building HTML slides! For small snippets you can cheat and include stuff in e.g. math environments, but in general this just leads to missing content.

\begin{alertblock}{Warning}
This is a Bad Thing
\end{alertblock}

\metroset{block=fill}

\begin{exampleblock}{Not Warning}
This is not a Bad Thing
\end{exampleblock}

\begin{block}{Welp}
I dunno if this is good or bad
\[M_\odot > M_\oplus\]
\end{block}


# Positive Demonstration

## Mixed Modes and Rotation

We have derived a semi-analytic description of rotation
in the presence of mode mixing.

. . .

Avoided crossings **must** be accounted for
to make credible statements about stellar rotation
and magnetic fields in evolved stars.

. . .

$$\mathrm{j}\mathrm{o}\mathrm{e}\mathrm{l}.\mathrm{o}\mathrm{n}\mathrm{g}\ \text{@}\ \text{yale}.\text{edu}$$

---

\begin{center}
\huge{\textsc{Negative Demonstration}}
\end{center}

---

\begin{center}
\huge{The End}\\\Large{Questions?} \\[2em] {\scriptsize Slides available at \url{https://gitlab.com/darthoctopus/yale-software-club/}}
\end{center}

## The End

[Slides available at <https://gitlab.com/darthoctopus/yale-software-club/>]{.small}
